﻿using System;
using Xunit;

namespace OpenMyGame.Tests
{
    public class PalindromeTests
    {
        [Theory]
        [InlineData("А роза упала на лапу Азора")]
        [InlineData("А в Енисее - синева")]
        [InlineData("А луна канула")]
        [InlineData("А муза рада музе без ума да разума")]
        public void IsPalindrome_ReturnTrue(string text)
        {
            Assert.True (Palindrome.IsPalindrome (text));
        }

        [Theory]
        [InlineData("Живу как черт в черном БМВ")]
        [InlineData("Двое сидели за решеткой: один видел грязь, второй звезды")]
        [InlineData("Вот кем был бы я, если бы не Божья милость")]
        [InlineData("")]
        public void IsPalindrome_ReturnFalse(string text)
        {
            Assert.False (Palindrome.IsPalindrome (text));
        }

        [Theory]
        [InlineData(null)]
        public void IsPalindrome_IfTextIsNull_ThrowArgumentNullException(string text)
        {
            Assert.Throws<ArgumentNullException> (
                () => Palindrome.IsPalindrome (text));
        }
    }
}

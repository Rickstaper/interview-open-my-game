﻿using System;
using Xunit;

namespace OpenMyGame.Tests
{
    public class BracketsSequenceTests
    {
        [Theory]
        [InlineData("{()[]}")]
        [InlineData("()[][{()}]")]
        [InlineData("10 + 2 * (13 + 4)")]
        [InlineData("(1 + 3) * (4 - 3)")]
        public void CheckBrackets_ReturnTrue(string parentheses)
        {
            Assert.True (BracketsSequence.CheckBrackets (parentheses));
        }

        [Theory]
        [InlineData ("{)(]}")]
        [InlineData ("[(]){}")]
        [InlineData("(")]
        [InlineData ("]")]
        [InlineData ("(1 + 3] + 8")]
        [InlineData("44 + 13 * (1 + 2))")]
        public void CheckBrackets_ReturnFalse (string parentheses)
        {
            Assert.False (BracketsSequence.CheckBrackets (parentheses));
        }

        [Theory]
        [InlineData(null)]
        public void CheckBrackets_IfSourceIsNull_ThrowArgumentNullException(string source)
        {
            Assert.Throws<ArgumentNullException> (
                () => BracketsSequence.CheckBrackets (source));
        }
    }
}

using System;
using Xunit;

namespace OpenMyGame.Tests
{
    public class LeapYearTests
    {
        [Theory]
        [InlineData(1764)]
        [InlineData (1812)]
        [InlineData (1924)]
        [InlineData (2012)]
        [InlineData (2020)]
        public void IsLeapYear_ReturnTrue(int year)
        {
            Assert.True (LeapYear.IsLeapYear(year));
        }

        [Theory]
        [InlineData (1763)]
        [InlineData (1919)]
        [InlineData (1927)]
        [InlineData (2007)]
        [InlineData (2043)]
        public void IsLeapYear_ReturnFalse (int year)
        {
            Assert.False (LeapYear.IsLeapYear (year));
        }


    }
}

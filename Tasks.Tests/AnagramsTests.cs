﻿using System;
using Xunit;

namespace OpenMyGame.Tests
{
    public class AnagramsTests
    {
        [Theory]
        [InlineData ("кот", "ток", "кто")]
        [InlineData("орк", "рок")]
        [InlineData("клоун", "уклон", "кулон")]
        [InlineData("раки", "каир", "ирак")]

        public void IsSameWords_True(params string[] words)
        {
            Assert.True (Anargams.IsSameWords (words));
        }

        [Theory]
        [InlineData("кот", "ток", "кТо")]
        [InlineData("джин", "жим", "жара")]
        [InlineData("клоун", "уклон", "курсач")]
        [InlineData ("раки", "каир", "лондон")]
        [InlineData("")]
        public void IsSameWords_False (params string[] words)
        {
            Assert.False (Anargams.IsSameWords (words));
        }

        [Theory]
        [InlineData(null)]
        public void IsSameWords_IfWordsIsNull_ThrowArgumentNullException (params string[] words)
        {  
            Assert.Throws<ArgumentNullException> (
                () => Anargams.IsSameWords(words));
        }
    }
}

﻿using System;

namespace OpenMyGame
{
    public static class BracketsSequence
    {
        public static bool CheckBrackets(string source)
        {
            if (source is null)
            {
                throw new ArgumentNullException (nameof (source));
            }

            if (source.Length == 0 || source.Length == 1)
            {
                return false;
            }

            for (int i = 0; i < source.Length -1; i++)
            {
                if (source[i] == '(' && (source[i + 1] == ']' || source[i + 1] == '}'))
                    return false;
                if (source[i] == '[' && (source[i + 1] == ')' || source[i + 1] == '}'))
                    return false;
                if (source[i] == '{' && (source[i + 1] == ']' || source[i + 1] == ')'))
                    return false;
            }

            if (!Brackets (source) || !Bracket(source) || !CurlyBracket(source))
                return false;

            return true;
        }

        /// <summary>
        /// Check open parenthesis "(" and close parenthesis ")"
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        private static bool Brackets(string source)
        {
            int count = 0;
            for (int i = 0; i < source.Length; i++)
            {
                if (source[i] == '(')
                    count++;
                if (source[i] == ')')
                    count--;
            }

            return count == 0;
        }

        /// <summary>
        /// Check open brace "{" and close brace "}"
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        private static bool CurlyBracket(string source)
        {
            int count = 0;
            for (int i = 0; i < source.Length; i++)
            {
                if (source[i] == '{')
                    count++;
                if (source[i] == '}')
                    count--;
            }

            return count == 0;
        }


        /// <summary>
        /// Check open bracket "[" and close bracket "]"
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        private static bool Bracket(string source)
        {
            int count = 0;
            for (int i = 0; i < source.Length; i++)
            {
                if (source[i] == '[')
                    count++;
                if (source[i] == ']')
                    count--;
            }

            return count == 0;
        }
    }
}

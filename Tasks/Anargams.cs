﻿using System;

namespace OpenMyGame

{
    public static class Anargams
    {
        public static bool IsSameWords(string[] source)
        {
            if (source is null)
            {
                throw new ArgumentNullException (nameof (source));
            }

            if (source[0].Length == 0)
            {
                return false;
            }

            char[] word = new char[source[0].Length];

            if (source.Length == 1)
            {
                return true;
            }

            for (int i = 0; i < source[0].Length; i++)
            {
                word[i] = source[0][i];
            }

            for (int i = 1; i < source.Length; i++)
            {
                if (word.Length != source[i].Length)
                {
                    return false;
                }

                foreach (char c in word)
                {
                    bool isWord = false;
                    for (int j = 0; j < source[i].Length; j++)
                    {
                        if (source[i][j] == c)
                        {
                            isWord = true;
                            break;
                        }
                    }

                    if (!isWord)
                    {
                        return false;
                    }
                }
            }

            return true;
        }
    }
}

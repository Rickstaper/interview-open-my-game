﻿using System;

namespace OpenMyGame
{
    public static class Palindrome
    {
        public static bool IsPalindrome(string source)
        {
            if (source is null)
            {
                throw new ArgumentNullException (nameof (source));
            }

            if (source.Length == 0)
            {
                return false;
            }

            string textWithoutSpace = string.Empty;

            for (int i = 0; i < source.Length; i++)
            {
                if ((source[i] >= 48 && source[i] <= 57) || (source[i] >= 1040 && source[i] <= 1071) || (source[i] >= 1072 && source[i] <= 1103))
                {
                    textWithoutSpace += source[i];
                }
            }

            for (int i = 0; i < textWithoutSpace.Length; i++)
            {
                if (textWithoutSpace[i] != textWithoutSpace[^(i + 1)] && textWithoutSpace[i] != textWithoutSpace[^(i + 1)] - 32 && textWithoutSpace[i] != textWithoutSpace[^(i + 1)] + 32)
                {
                    return false;
                }
            }

            return true;
        }
    }
}
